﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using WebApi.Dtos;
using WebApi.Helpers;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [Route("/api/Drugstores")]
    public class DrugstoresController : Controller
    {
        private readonly AppSettings _appSettings;
        private IDrugstoreService _drugstoreService;
        private IUserService _userService;

        public DrugstoresController(
            IDrugstoreService drugstoreService, 
            IUserService userService,
            IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
            _drugstoreService = drugstoreService;
            _userService = userService;
        }


        [HttpGet]
        public IActionResult GetlAll()
        {
            var drugstores = _drugstoreService.GetAll();
            return Ok(drugstores);
        }

        [HttpGet("{id}/medicaments")]
        public IActionResult GetDrugs(int id)
        {
            var drugs = _drugstoreService.GetDrugs(id);
            return Ok(drugs);
        }

        // GET: api/Drugstores/5
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var drugstore = _drugstoreService.GetById(id);
            return Ok(drugstore);
        }

        // POST: api/Drugstores
        [HttpPost]
        public IActionResult Create([FromBody]string medicament)
        {
            return Ok();
        }

        // PUT: api/Drugstores/5
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]MedicamentDto medicament)
        {
            return Ok();
        }

        // DELETE: api/Drugstores/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _drugstoreService.Delete(id);
            return Ok();
        }

        [HttpPost("{id}/voters")]
        public ActionResult AddVoter(int id, [FromQuery]string voterName)
        {
            if( !_userService.GetByUsername(voterName)){
                return NotFound(voterName);
            }

            if (voterName.Equals("") && voterName == null)
                return BadRequest("Username is not valid!");

            if (_drugstoreService.AddVoter(id, voterName)) {
                return Ok(voterName + " a fost adaugat");
            }

            return BadRequest("Duplicated user!");
        }

        [HttpDelete("{id}/voters")]
        public ActionResult RemoveVoter(int id, [FromQuery]string voterName)
        {
            if (!_userService.GetByUsername(voterName))
            {
                return NotFound(voterName);
            }

            if (voterName.Equals("") && voterName == null)
                return BadRequest("Username is not valid!");

            if (_drugstoreService.RemoveVoter(id, voterName))
            {
                return Ok(voterName + " a fost sters din lista");
            }

            return BadRequest("Voter Not found!");
        }



    }
}