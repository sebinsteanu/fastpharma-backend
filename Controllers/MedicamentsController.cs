﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Dtos;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [Route("/api/Medicaments")]
    public class MedicamentsController : Controller
    {

        private readonly AppSettings _appSettings;
        private IMapper _mapper;
        private IMedicamentsService _medicamentsService;
        private IUserService _userService;

        public MedicamentsController(
            IOptions<AppSettings> appSettings,
            IMapper mapper,
            IMedicamentsService medicamentsService,
            IUserService userService
            )
        {
            _appSettings = appSettings.Value;
            _mapper = mapper;
            _medicamentsService = medicamentsService;
            _userService = userService;
        }


        // GET: api/Medicaments
        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_medicamentsService.GetAll());
        }

        // POST: api/Medicaments/{id}/order
        [HttpGet("{medId}/order")]
        public IActionResult AddToCart(int medId, [FromQuery] int quantity)
        {
            string id = User.Identity.Name;
            var foundUser = _userService.GetById(int.Parse(id));
            if (foundUser == null)
                return Unauthorized();

            var order = new Order();
            order.MedicamentId = medId;
            order.Quantity = quantity;

            if (!_medicamentsService.AddOrder(foundUser, order))
                return BadRequest();

            return Ok();
        }

        //// GET: api/Medicaments/drugstore?drugstoreId={drugstoreId}
        //[HttpGet("drugstore")]
        //public IActionResult GetFromDrugstore([FromQuery] int drugstoreId)
        //{
        //    var drugs = _mapper.Map<IList<MedicamentDto>>(_medicamentsService.GetFromDrugstore(drugstoreId));
        //    return Ok(drugs);
        //}

        [HttpGet("{id}/prospect")]
        public IActionResult GetProspect(int id)
        {
            return Ok(_medicamentsService.getProspect(id));
        }


        // GET: api/Medicaments/5
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            return Ok(_medicamentsService.GetById(id));
        }
        
        // POST: api/Medicaments
        [HttpPost]
        public IActionResult Create([FromBody]Medicament medicament)
        {
            return Ok();
        }
        
        // PUT: api/Medicaments/5
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]Medicament medicament)
        {
            return Ok();
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            return Ok();
        }
    }
}
