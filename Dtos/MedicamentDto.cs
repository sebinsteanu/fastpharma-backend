﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Dtos
{
    public class MedicamentDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }

        public int DrugstoreId { get; set; }
    }
}
