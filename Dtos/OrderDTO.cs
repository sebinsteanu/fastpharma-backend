﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Dtos
{
    public class OrderDto
    {
        public int MedId { get; set; }
        public int Quantity {get;set; }
    }
}
