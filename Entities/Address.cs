﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class Address
    {
        public int Id { get; set; }

        [StringLength(250)]
        public string City { get; set; }

        public string Street { get; set; }
        public int Number { get; set; }
        
        
        public float Latitude { get; set; }
        public float Longitude { get; set; }
    }
}
