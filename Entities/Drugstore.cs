﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class Drugstore
    {
        public int Id { get; set; }

        [StringLength(250)]
        public string Name { get; set; }

        [Range(0, 10)]
        public short Rating { get; set; }
        public string Phone { get; set; }
        public string OpeningAt { get; set; }
        public string ClosingAt { get; set; }
        public string Logo { get; set; }
        public string Website { get; set; }

        public int AddressId { get; set; }
        public Address Address { get; set; }


        [NotMapped]
        private List<string> VotersStorage { get; set; } = new List<string>();


        public string Voters
        {
            get
            {
                return VotersStorage == null || !VotersStorage.Any()
                           ? null
                           : JsonConvert.SerializeObject(VotersStorage);
            }

            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    VotersStorage.Clear();
                else
                    VotersStorage = JsonConvert.DeserializeObject<List<string>>(value);
            }
        }

    }
}
