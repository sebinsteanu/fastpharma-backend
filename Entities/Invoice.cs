﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    //all orders of a single user   
    public class Invoice
    {
        [Key]
        public int Id { get; set; }

        public bool Executed { get; set; }
        public bool Sent { get; set; }
        public Double Total { get; set; }
        public DateTime DataExec { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        //public Address Address {get;set;}
        //public int AddressId { get; set; }

    }
}
