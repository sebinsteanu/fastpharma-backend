﻿namespace WebApi.Entities
{
    public class Medicament
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Quantity { get; set; }
        public string Weight { get; set; }
        public string DiseaseTarget { get; set; }
        public string ImageUrl { get; set; }
        public double Price { get; set; }


        public int DrugstoreId { get; set; }
        public Drugstore Drugstore {get;set;}
        
        public int? ProducerId { get; set; }
        public Producer Producer { get; set; }
    }
}
