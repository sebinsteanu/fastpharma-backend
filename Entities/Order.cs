﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    //an order for a single medicament
    public class Order
    {
        [Key]
        public int Id { get; set; }
        public int Quantity { get; set; }

        public Medicament Medicament { get; set; }
        public int MedicamentId { get; set; }

        public Invoice Invoice { get; set; }
        public int InvoiceId { get; set; }

    }
}
