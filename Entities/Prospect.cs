﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class Prospect
    {
        public int Id { get; set; }
        public string Descriprion { get; set; }
        public string UseMode { get; set; }
        public string ActionMode { get; set; }
        public string Composition { get; set; }
        public string Warnings { get; set; }
        public string Risks { get; set; }
        public string Benefits { get; set; }



        public Medicament Medicament { get; set; }
        public int MedicamentId { get; set; }

    }
}
