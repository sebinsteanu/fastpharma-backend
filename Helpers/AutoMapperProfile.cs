using AutoMapper;
using WebApi.Dtos;
using WebApi.Entities;

namespace WebApi.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>().ForMember(c => c.Id, opt => opt.Ignore());


            CreateMap<Medicament, MedicamentDto>();
            CreateMap<MedicamentDto, Medicament>().ForMember(c => c.Id, opt => opt.Ignore());

            CreateMap<Order, OrderDto>();
            CreateMap<OrderDto, Order>().ForMember(o => o.Id, opt => opt.Ignore());
        }
    }
}