using Microsoft.EntityFrameworkCore;
using WebApi.Entities;

namespace WebApi.Helpers
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }

        public DbSet<Address> Addresses { get; set; }
        public DbSet<Medicament> Medicaments { get; set; }
        public DbSet<Drugstore> Drugstores { get; set; }
        public DbSet<Prospect> Prospects { get; set; }
        public DbSet<Producer> Producers { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<Order> Orders { get; set; }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Order>().HasKey(o => new {o.InvoiceId, o.UserId });
        //}
    }
}