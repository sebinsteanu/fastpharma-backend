﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WebApi.Migrations
{
    public partial class Drugstoresupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Drugstores",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Drugstores_UserId",
                table: "Drugstores",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Drugstores_Users_UserId",
                table: "Drugstores",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Drugstores_Users_UserId",
                table: "Drugstores");

            migrationBuilder.DropIndex(
                name: "IX_Drugstores_UserId",
                table: "Drugstores");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Drugstores");
        }
    }
}
