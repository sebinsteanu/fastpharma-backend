﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WebApi.Migrations
{
    public partial class idfarmptfarmacisti : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Drugstores_Users_UserId",
                table: "Drugstores");

            migrationBuilder.DropIndex(
                name: "IX_Drugstores_UserId",
                table: "Drugstores");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Drugstores");

            migrationBuilder.AddColumn<int>(
                name: "DrugstoreId",
                table: "Users",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Users_DrugstoreId",
                table: "Users",
                column: "DrugstoreId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Drugstores_DrugstoreId",
                table: "Users",
                column: "DrugstoreId",
                principalTable: "Drugstores",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Drugstores_DrugstoreId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_DrugstoreId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "DrugstoreId",
                table: "Users");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Drugstores",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Drugstores_UserId",
                table: "Drugstores",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Drugstores_Users_UserId",
                table: "Drugstores",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
