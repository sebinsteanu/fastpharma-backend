﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WebApi.Migrations
{
    public partial class tryfix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Users_DrugstoreId",
                table: "Users",
                column: "DrugstoreId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Drugstores_DrugstoreId",
                table: "Users",
                column: "DrugstoreId",
                principalTable: "Drugstores",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Drugstores_DrugstoreId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_DrugstoreId",
                table: "Users");
        }
    }
}
