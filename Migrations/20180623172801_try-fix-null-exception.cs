﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WebApi.Migrations
{
    public partial class tryfixnullexception : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Drugstores_DrugstoreId",
                table: "Users");

            migrationBuilder.AlterColumn<int>(
                name: "DrugstoreId",
                table: "Users",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Drugstores_DrugstoreId",
                table: "Users",
                column: "DrugstoreId",
                principalTable: "Drugstores",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Drugstores_DrugstoreId",
                table: "Users");

            migrationBuilder.AlterColumn<int>(
                name: "DrugstoreId",
                table: "Users",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Drugstores_DrugstoreId",
                table: "Users",
                column: "DrugstoreId",
                principalTable: "Drugstores",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
