﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WebApi.Migrations
{
    public partial class addedvotersfordrugstores2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "VotersJsonForDb",
                table: "Drugstores");

            migrationBuilder.AddColumn<string>(
                name: "Voters",
                table: "Drugstores",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Voters",
                table: "Drugstores");

            migrationBuilder.AddColumn<string>(
                name: "VotersJsonForDb",
                table: "Drugstores",
                nullable: true);
        }
    }
}
