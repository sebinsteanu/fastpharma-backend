﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WebApi.Migrations
{
    public partial class addedmedicaments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Medicaments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DrugstoreId",
                table: "Medicaments",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ImageUrl",
                table: "Medicaments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Medicaments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "Medicaments",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Medicaments_DrugstoreId",
                table: "Medicaments",
                column: "DrugstoreId");

            migrationBuilder.AddForeignKey(
                name: "FK_Medicaments_Drugstores_DrugstoreId",
                table: "Medicaments",
                column: "DrugstoreId",
                principalTable: "Drugstores",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Medicaments_Drugstores_DrugstoreId",
                table: "Medicaments");

            migrationBuilder.DropIndex(
                name: "IX_Medicaments_DrugstoreId",
                table: "Medicaments");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Medicaments");

            migrationBuilder.DropColumn(
                name: "DrugstoreId",
                table: "Medicaments");

            migrationBuilder.DropColumn(
                name: "ImageUrl",
                table: "Medicaments");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Medicaments");

            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "Medicaments");
        }
    }
}
