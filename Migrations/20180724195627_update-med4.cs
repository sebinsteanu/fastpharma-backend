﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WebApi.Migrations
{
    public partial class updatemed4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProducerId",
                table: "Medicaments",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Medicaments_ProducerId",
                table: "Medicaments",
                column: "ProducerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Medicaments_Producers_ProducerId",
                table: "Medicaments",
                column: "ProducerId",
                principalTable: "Producers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Medicaments_Producers_ProducerId",
                table: "Medicaments");

            migrationBuilder.DropIndex(
                name: "IX_Medicaments_ProducerId",
                table: "Medicaments");

            migrationBuilder.DropColumn(
                name: "ProducerId",
                table: "Medicaments");
        }
    }
}
