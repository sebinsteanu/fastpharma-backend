﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Entities;
using WebApi.Helpers;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Services
{
    public interface IDrugstoreService
    {
        IEnumerable<Drugstore> GetAll();
        IEnumerable<Medicament> GetDrugs(int drugstoreId);
        Drugstore GetById(int id);
        Drugstore Create(Drugstore drugstore);
        void Update(int id, string voterName);
        void Delete(int id);
        bool AddVoter(int id, string voterName);
        bool RemoveVoter(int id, string voterName);
    }


    public class DrugstoreService : IDrugstoreService
    {
        private DataContext _context;

        public DrugstoreService(DataContext dataContext)
        {
            _context = dataContext;
        }

        public Drugstore Create(Drugstore drugstore)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            var drugstore = _context.Drugstores.Find(id);
            if(drugstore != null)
            {
                _context.Drugstores.Remove(drugstore);
            }
        }

        public IEnumerable<Drugstore> GetAll()
        {
            return _context.Drugstores.Include(d => d.Address).ToList();
        }

        public Drugstore GetById(int id)
        {
            return _context.Drugstores.Include(d => d.Address).ToList().Find(d => d.Id == id);
        }

        public IEnumerable<Medicament> GetDrugs(int drugstoreId)
        {
            return _context.Medicaments.Where(med => med.DrugstoreId == drugstoreId).ToList();
        }

        public bool AddVoter(int id, string voterName)
        {
            var drugstoreDB = _context.Drugstores.Find(id);

            if (drugstoreDB != null)
            {
                var drugstoreVoters = drugstoreDB.Voters != null ? JsonConvert.DeserializeObject<List<string>>(drugstoreDB.Voters) : new List<string>();

                var duplicatedVoter = drugstoreVoters.Find(voter => voter.Equals(voterName));
                if (duplicatedVoter != null)
                {
                    return false;
                }

                drugstoreVoters.Add(voterName);
                drugstoreDB.Voters = JsonConvert.SerializeObject(drugstoreVoters);

                var voters2 = JsonConvert.DeserializeObject<List<string>>(drugstoreDB.Voters);

                _context.Drugstores.Update(drugstoreDB);
                _context.SaveChanges();
            }
            return true;
        }

        public bool RemoveVoter(int id, string voterName)
        {
            var drugstoreDB = _context.Drugstores.Find(id);

            if (drugstoreDB != null)
            {
                var drugstoreVoters = drugstoreDB.Voters != null ? JsonConvert.DeserializeObject<List<string>>(drugstoreDB.Voters) : new List<string>();

                var foundVoter = drugstoreVoters.Find(voter => voter.Equals(voterName));
                if (foundVoter == null)
                {
                    return false;
                }

                drugstoreVoters.Remove(voterName);
                drugstoreDB.Voters = JsonConvert.SerializeObject(drugstoreVoters);
                
                _context.Drugstores.Update(drugstoreDB);
                _context.SaveChanges();
            }
            return true;
        }

        public void Update(int id, string voterName)
        {
            throw new NotImplementedException();
        }
    }
}
