﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{


    public interface IMedicamentsService
    {
        IEnumerable<Medicament> GetAll();
        Medicament GetById(int id);
        Medicament Create(Medicament medicament);
        Medicament Update(int id, Medicament medicament);
        void Delete(int id);
        bool AddVoter(int id, string voterName);
        bool RemoveVoter(int id, string voterName);
        bool AddOrder(User user, Order order);
        Prospect getProspect(int medId);
    }


    public class MedicamentsService : IMedicamentsService
    {
        private DataContext _context;

        public MedicamentsService(DataContext dataContext)
        {
            _context = dataContext;
        }

        public bool AddVoter(int id, string voterName)
        {
            throw new NotImplementedException();
        }

        public Medicament Create(Medicament medicament)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Medicament> GetAll()
        {
            return _context.Medicaments.ToList();
        }


        public Medicament GetById(int id)
        {
            return _context.Medicaments.Find(id);
        }

        public bool RemoveVoter(int id, string voterName)
        {
            throw new NotImplementedException();
        }

        public Medicament Update(int id, Medicament medicament)
        {
            throw new NotImplementedException();
        }

        public Prospect getProspect(int medId)
        {
            return _context.Prospects.Where(prosp => prosp.MedicamentId == medId).SingleOrDefault();
        }

        public bool AddOrder(User user, Order order)
        {
            Medicament med = _context.Medicaments.Find(order.MedicamentId);
            if (med == null)
                return false;

            var invoice = _context.Invoices.Where(i => i.Executed == false && i.Sent == false).SingleOrDefault(i => i.UserId == user.Id);
            if (invoice == null)
            {
                invoice = new Invoice();
                invoice.UserId = user.Id;
                _context.Invoices.Add(invoice);
                _context.SaveChanges();
            }

            var existingOrder = _context.Orders.SingleOrDefault(o => o.InvoiceId == invoice.Id && o.MedicamentId == med.Id);
            if (existingOrder != null)
            {
                int newQuantity = existingOrder.Quantity + order.Quantity;
                existingOrder.Quantity = newQuantity > 0 ? newQuantity : existingOrder.Quantity;
                _context.Orders.Update(existingOrder);
            }
            else if (order.Quantity > 0)
            {
                var newOrder = new Order();
                newOrder.InvoiceId = invoice.Id;
                newOrder.MedicamentId = med.Id;
                newOrder.Quantity = order.Quantity;
                _context.Orders.Add(newOrder);
            }
            invoice.Total += order.Quantity * med.Price;

            _context.Update(invoice);
            _context.SaveChanges();

            return true;
        }
    }
}
