using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.Entities;
using WebApi.Helpers;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Services
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
        IEnumerable<User> GetAllUsers();
        User GetById(int id);
        User Create(User user, string password);
        void Update(User user, string password = null);
        void Delete(int id);
        bool GetByUsername(string voterName);

        Invoice GetActiveInvoice(int userId);
        IEnumerable<Order> GetActiveInvoiceOrders(int userId);
        IEnumerable<Invoice> GetPastInvoices(int userId);
        void GetActiveInvoiceOrdersCount(int userId, out int count);
        bool CheckOut(int userId);
        bool CancelOrder(int userId, int invoiceId);

    }

    public class UserService : IUserService
    {
        private DataContext _context;

        public UserService(DataContext context)
        {
            _context = context;
        }

        public User Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            var user = _context.Users.SingleOrDefault(x => x.Username == username);

            // check if username exists
            if (user == null)
                return null;

            // check if password is correct
            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            // authentication successful
            return user;
        }

        public IEnumerable<User> GetAllUsers()
        {
            return _context.Users;
        }

        public User GetById(int id)
        {
            return _context.Users.Find(id);
        }

        public User Create(User user, string password)
        {
            // validation
            if (string.IsNullOrWhiteSpace(password))
                throw new AppException("Password is required");

            if (_context.Users.Any(x => x.Username == user.Username))
                throw new AppException("Username " + user.Username + " is already taken");

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            _context.Users.Add(user);
            _context.SaveChanges();

            return user;
        }

        public void Update(User userParam, string password = null)
        {
            var user = _context.Users.Find(userParam.Id);

            if (user == null)
                throw new AppException("User not found");

            if (userParam.Username != user.Username)
            {
                // username has changed so check if the new username is already taken
                if (_context.Users.Any(x => x.Username == userParam.Username))
                    throw new AppException("Username " + userParam.Username + " is already taken");
            }

            // update user properties
            user.FirstName = userParam.FirstName;
            user.LastName = userParam.LastName;
            user.Username = userParam.Username;

            // update password if it was entered
            if (!string.IsNullOrWhiteSpace(password))
            {
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(password, out passwordHash, out passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
            }

            _context.Users.Update(user);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = _context.Users.Find(id);
            if (user != null)
            {
                _context.Users.Remove(user);
                _context.SaveChanges();
            }
        }

        #region invoices
        public IEnumerable<Order> GetActiveInvoiceOrders(int userId)
        {
            var invoice = GetActiveInvoice(userId);
            if (invoice != null)
            {
                int invoiceId = invoice.Id;

                return _context.Orders.Include(o => o.Medicament).Where(o => o.InvoiceId == invoiceId).ToList();
            }
            return null;
        }

        public IEnumerable<Invoice> GetPastInvoices(int userId)
        {
            return _context.Invoices.Where(i => i.UserId == userId && i.Sent == true );
        }

        public void GetActiveInvoiceOrdersCount(int userId, out int count)
        {
            count = GetActiveInvoiceOrders(userId).Count();
        }

        public bool CheckOut(int userId)
        {
            var invoice = GetActiveInvoice(userId);
            if (invoice != null && invoice.Sent == false)
            {
                invoice.Sent = true;
                invoice.DataExec = DateTime.Now;
                _context.Invoices.Update(invoice);
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool CancelOrder(int userId, int invoiceId)
        {
            var invoice = _context.Invoices.Where(i => i.Id == invoiceId).FirstOrDefault();
            if (invoice != null && invoice.UserId == userId)
            {
                _context.Invoices.Remove(invoice);
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        #endregion

        #region private helper methods

        public bool GetByUsername(string voterName)
        {
            var userFound = _context.Users.Where(u => u.Username == voterName).FirstOrDefault();
            return userFound != null;
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }

        public Invoice GetActiveInvoice(int userId)
        {
            return _context.Invoices.Where(i => i.UserId == userId && i.Sent == false).FirstOrDefault();
        }
        #endregion
    }
}